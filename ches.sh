#!/bin/sh

# Eerste versies van het scriptje was een fen naar ascii converter. Heb daarna geprobeerd van de code een bash schaak app te maken. Na scope range problemen besloten het scriptje weer wat in te korten. Dit script is een bash schaakbord. Zetten ingeven met uci notatie. Bord kent geen schaakregels.
# TOEVOEGEN: Hoe plaats je stukken op het bord? Volgens mij is het op dit moment alleen mogelijk om stukken te verplaatsen.
# TO DO: HUD minder hard coded maken/opfrissen.  


VERSIE="PRE ALPHA VERSIE      "
fenarray=()
zetcounter=10
velden=( a8 b8 c8 d8 e8 f8 g8 h8 a7 b7 c7 d7 e7 f7 g7 h7 a6 b6 c6 d6 e6 f6 g6 h6 a5 b5 c5 d5 e5 f5 g5 h5 a4 b4 c4 d4 e4 f4 g4 h4 a3 b3 c3 d3 e3 f3 g3 h3 a2 b2 c2 d2 e2 f2 g2 h2 a1 b1 c1 d1 e1 f1 g1 h1 )

function kleur { # kleuren.
#	TO DO: script moet lezen of terminal 256 of 8 kleuren support en te kiezen kleuren in het scriptje daaraan aanpassen.
case "$1" in
"witv")
	printf "\033[48;5;223m"
;;
"zwartv")
	printf "\033[48;5;166m"
;;
"witst")
	printf "\033[48;5;255m"
;;
"witsh")
	printf "\033[48;5;249m"
;;
"zwartst")
	printf "\033[48;5;16m"
;;
"zwartsh")
	printf "\033[48;5;8m"
;;
"ztypekl")
	printf "\033[48;5;6m"
;;
"wtypekl")
	printf "\033[48;5;4m"
;;
"hud")
	printf "\033[38;5;1m"
;;
"default")
	printf "\033[0m"
;;
esac
}

function tekstgen { # Genereert tekst in gewenste tekstblokken.
# INPUT: $1=begincursor-y; $2=begincursor-x; $3=max karakters in veld; $4=tekst;
# Niet vergeten "" om positional variabelen te plaatsen.
local IFS=$'\n'
arr=( $4 )
N=${#arr[@]}
printf "\033[${1};${2}H"
for ((y = 0; y <= N; y++ )); do
	printf "\033[1B\033[${M}D" 
	M=${#arr[$y]}
	printf "${arr[$y]}"
done
printf "\033[52;${3}H"
}

function quit { clear; printf "\033[0m"; exit ;}

function readuci { #	Functie leest 5 karakters en slaat deze op.
#	OUTPUT: $uci
#	Als een uci met ongeldige opmaak ingevoerd wordt, geeft command een error en vraagt wederom om invoer.
#	BUG: BIJ ONGELDIGE UCI VERDWIJNT HET STUK DAT DE VORIGE ZET VERPLAATST IS SOMS.
#	BUG: LEGE STUKKEN KUNNEN NU OOK VERPLAATST WORDEN.
#	Mogelijkheid om command te onderbreken bestaat nog niet.
while [ : ]
do 
	tekstgen 51 0 25 "Voer een uci zet in:"
	read uci
	if [[ ${#uci} = 4 ]] && [[ ${uci:0:1} =~ [a-h] ]] && [[ ${uci:1:1} =~ [1-8] ]] && [[ ${uci:2:1} =~ [a-h] ]] && [[ ${uci:3:1} =~ [1-8] ]] && [[ ${uci:0:2} != ${uci:2:2} ]] 
	then
		startv="${uci:0:2}"
		doelv="${uci:2:2}"
		break
	elif [[ ${#uci} == 5 ]] && [[ "${uci:4:1}" =~ [rnbqRNBQ] ]]
	then
		startv="${uci:0:2}"	
		doelv="${uci:2:2}"
		promotie="${uci:4:1}"
		break
	else
	tekstgen 50 0 25 "ONGELDIGE UCI      "
	fi
done
}

function fenuciman { #	Functie neemt fen in STRINGA en wijzigt deze aan de hand van uci.
#	OUTPUT: $STRINGC
for i in {0..63}
do
	inPOS[$i]=${STRINGA:$i:1}
done
for i in {0..63}
do
	case "$startv" in 
	"${velden[i]}")
		starts="${inPOS[i]}" 
		STRINGB="${STRINGA:0:$[$i]}0${STRINGA:$i+1}"
		replace=()
			for p in {0..63}
			do
				case "$doelv" in
				"${velden[p]}")
					if [ ${#promotie} = 0 ]	
					then					
						STRINGC="${STRINGB:0:$p}${starts}$replace${STRINGB:$p+1}"
						replace=()
					else
						STRINGC="${STRINGB:0:$p}$promotie${STRINGB:$p+1}"
						promotie=()
					fi
				;;
				esac
			done
	;;
	esac
done
}

function appendhistory { fenarray+=("$STRINGA");}

function oldnew { STRINGA="${STRINGC}"; STRINGC=();}

function genhud { # Geneert de hud van ches.sh.
kleur hud
for i in {1..50}
do    
	if [ $i = 1 ]
	then 
		for r in {0..148}
		do
			printf "\033[1;${r}H═"
		done
	elif [ $i = 9 ]
	then
		printf "\033[${i};0H║"		
		for r in {114..148}
		do
			printf "\033[9;${r}H═"
		done
	elif [ $i = 50 ]
	then
		for r in {0..148}
		do
			printf "\033[50;${r}H═"
		done
	else
		printf "\033[${i};0H║"
		printf "\033[${i};114H║"
		printf "\033[${i};148H║"
	fi	    
	printf "\033[1;0H╔"
	printf "\033[1;114H╦"
	printf "\033[1;148H╗"
	printf "\033[9;114H╠"
	printf "\033[9;148H╣"
	printf "\033[50;0H╚" 
	printf "\033[50;114H╩"
	printf "\033[50;148H╝"
done
kleur default
}

function zetlijst {
if [[ "$starts" =~ [A-Z] ]]
then
	printf "\033[0m\033[$zetcounter;115H""$1"
elif [[ "$starts" =~ [a-z] ]]
then
	printf "\033[0m\033[$zetcounter;121H""$1"
	zetcounter=$(( zetcounter + 1 ))

fi		
}

function genbord {
q=0; printf "\033[2;2H"
for i in {0..7}
do
	for p in {0..7}
	do
		S=$(((i+p)%2))
			if [ $S != 0 ]
			then
				for t in {0..5}
				do
					kleur zwartv
					x=$(( p * 14 + 2 ))
					y=$(( q + t + 2 ))
					printf "\033[${y};${x}H"
					printf "              "
					kleur default
				done
			else
				for t in {0..5}
				do 
					kleur witv
					x=$(( p * 14 + 2 ))
					y=$(( q + t + 2 ))
					printf "\033[${y};${x}H"
					printf "              "
					kleur default
				done
			fi
	done
q=$(( q + 6 ))
done
}              

function fenstring { # neemt variabele $fen en ontleedt deze in 64 karakters en slaat uitvoer op in variabele $STRINGA.
#	OUTPUT: $STRINGA
#	Een karakater voor ieder veld van het bord.

STRINGA=$(echo "$fen"); STRINGA=$(echo "${STRINGA//\/}"); STRINGA=$(echo "${STRINGA//1/0}"); STRINGA=$(echo "${STRINGA//2/00}"); STRINGA=$(echo "${STRINGA//3/000}"); STRINGA=$(echo "${STRINGA//4/0000}"); STRINGA=$(echo "${STRINGA//5/00000}"); STRINGA=$(echo "${STRINGA//6/000000}"); STRINGA=$(echo "${STRINGA//7/0000000}"); STRINGA=$(echo "${STRINGA//8/00000000}");}

function genstukken { 
q=0
for i in {0..7}
do
	for p in {0..7}
	do
		POS[$(( p + q ))]=${1:$(( p + q )):1}
	done
q=$(( q + 8 ))
done
for i in {0..7}
do 
	for p in {0..7}
	do 
		case "${POS[$(( i * 8 + p ))]}" in
		"p")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur zwartsh
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur default
		;;
		"P")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witst
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur witsh
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur default
		;;
		"N")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witsh
			printf "\033[${y};${x}H\033[1B\033[7C  "
			printf "\033[${y};${x}H\033[2B\033[6C "
			printf "\033[${y};${x}H\033[2B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[6C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur witst
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[8C  "
			kleur wtypekl
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[7C "
			kleur default
		;;
		"n")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[1B\033[7C  "
			printf "\033[${y};${x}H\033[2B\033[6C "
			printf "\033[${y};${x}H\033[2B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[6C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur zwartsh
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[8C  "
			kleur ztypekl
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[7C "
			kleur default
		;;
		"b")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur zwartsh
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur ztypekl
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur default
		;;
		"B")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witst
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur witsh
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur wtypekl
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur default
		;;
		"r")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[1B\033[5C     "
			printf "\033[${y};${x}H\033[2B\033[6C   "
			kleur zwartsh
			printf "\033[${y};${x}H\033[1B\033[6C "
			printf "\033[${y};${x}H\033[1B\033[8C "   
			printf "\033[${y};${x}H\033[3B\033[6C   " 
			kleur ztypekl
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[5C     "
			kleur default
		;;
		"R")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witst
			printf "\033[${y};${x}H\033[1B\033[5C     "
			printf "\033[${y};${x}H\033[2B\033[6C   "
			kleur witsh
			printf "\033[${y};${x}H\033[1B\033[6C "
			printf "\033[${y};${x}H\033[1B\033[8C "   
			printf "\033[${y};${x}H\033[3B\033[6C   " 
			kleur wtypekl
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[5C     "
			kleur default
		;;
		"q")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur zwartsh
			printf "\033[${y};${x}H\033[3B\033[6C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[5C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur ztypekl
			printf "\033[${y};${x}H\033[5C "
			printf "\033[${y};${x}H\033[7C "
			printf "\033[${y};${x}H\033[9C "
			printf "\033[${y};${x}H\033[1B\033[6C "
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[2B\033[7C "
			kleur default
		;;
		"Q")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witst
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[4B\033[6C   "
			kleur witsh
			printf "\033[${y};${x}H\033[3B\033[6C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[5C "
			printf "\033[${y};${x}H\033[4B\033[9C "
			kleur wtypekl
			printf "\033[${y};${x}H\033[5C "
			printf "\033[${y};${x}H\033[7C "
			printf "\033[${y};${x}H\033[9C "
			printf "\033[${y};${x}H\033[1B\033[6C "
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[2B\033[7C "
			kleur default
		;;
		"k")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur zwartst
			printf "\033[${y};${x}H\033[8C "
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[7C   "
			kleur zwartsh
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[1B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[6C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur ztypekl
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[2B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			kleur default
		;;
		"K")
			y=$(( i * 6 + 2 ))
			x=$(( p * 14 + 1))
			kleur witst
			printf "\033[${y};${x}H\033[8C "
			printf "\033[${y};${x}H\033[2B\033[8C "
			printf "\033[${y};${x}H\033[4B\033[7C   "
			kleur witsh
			printf "\033[${y};${x}H\033[1B\033[7C "
			printf "\033[${y};${x}H\033[1B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[7C "
			printf "\033[${y};${x}H\033[3B\033[9C "
			printf "\033[${y};${x}H\033[4B\033[6C "
			printf "\033[${y};${x}H\033[4B\033[10C "
			kleur wtypekl
			printf "\033[${y};${x}H\033[1B\033[8C "
			printf "\033[${y};${x}H\033[2B\033[7C "
			printf "\033[${y};${x}H\033[2B\033[9C "
			printf "\033[${y};${x}H\033[3B\033[8C "
			kleur default
		;;
		esac
	done
done
}

function asciibord { numc=$(echo -n "${OUTPUT[$i]}" | wc -c); kp=$(( numc * 8 )); local IFS=''; echo -n "${OUTPUT[*]}" | sed "s/.\{$kp\}/&\n/g"; }

function fenascii {
read filename
iq=0; for i in {1..8};{ RIJ[$i]="${STRINGA:$iq:8}"; iq=$[$iq+8];}
	for p in {1..8};{ for i in {1..8};{
		S=$(((p+i)%2))		
		ip=$[$i-1]
		POS[$i]="${RIJ[$p]:$ip:1}"
			if [ $S != 0 ]
			then
			case "${POS[$i]}" in
			"0")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="............."
			OUTPUT[$i+16]="............."
			OUTPUT[$i+24]="............."
			OUTPUT[$i+32]="............."
			OUTPUT[$i+40]="............."
			;;
			"p")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="......@......"
			OUTPUT[$i+16]=".....(@)....."
			OUTPUT[$i+24]=".....(@)....."
			OUTPUT[$i+32]="..../@@@\...."
			OUTPUT[$i+40]="............."
			;;
			"P")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="......0......"
			OUTPUT[$i+16]=".....( )....."
			OUTPUT[$i+24]=".....( )....."
			OUTPUT[$i+32]="..../   \...."
			OUTPUT[$i+40]="............."
			;;
			"R")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="....[]_[]...."
			OUTPUT[$i+16]="....\   /...."
			OUTPUT[$i+24]=".....| |....."
			OUTPUT[$i+32]=".....| |....."
			OUTPUT[$i+40]="..../___\...."
			;;
			"B")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]=".....(/)....."
			OUTPUT[$i+16]="...../ \....."
			OUTPUT[$i+24]="....(   )...."
			OUTPUT[$i+32]=".....) (....."
			OUTPUT[$i+40]="..../   \...."
			;;
			"N") 
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="..._/~~~\...."
			OUTPUT[$i+16]="..[__   }...."
			OUTPUT[$i+24]=".....\ (....."
			OUTPUT[$i+32]=".....| |....."
			OUTPUT[$i+40]="..../   \...."
			;;
			"r")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="....[].[]...."
			OUTPUT[$i+16]="....\@@@/...."
			OUTPUT[$i+24]=".....|@|....."
			OUTPUT[$i+32]=".....|@|....."
			OUTPUT[$i+40]="..../@@@\...."
			;;
			"b")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]=".....(/)....."
			OUTPUT[$i+16]="...../@\....."
			OUTPUT[$i+24]="....(@@@)...."
			OUTPUT[$i+32]=".....)@(....."
			OUTPUT[$i+40]="..../@@@\...."
			;;
			"n")
			   OUTPUT[$i]="............."
			 OUTPUT[$i+8]="..._/@@@\...."
			OUTPUT[$i+16]="..[@@@@@}...."
			OUTPUT[$i+24]=".....\@(....."
			OUTPUT[$i+32]=".....|@|....."
			OUTPUT[$i+40]="..../@@@\...."
			;;
			"q")
			   OUTPUT[$i]=".....www....."
			 OUTPUT[$i+8]=".....)@(....."
			OUTPUT[$i+16]="....{@@@}...."
			OUTPUT[$i+24]=".....|@|....."
			OUTPUT[$i+32]=".....)@(....."
			OUTPUT[$i+40]="..../@@@\...."
			;;
			"Q")
			   OUTPUT[$i]=".....www....."
			 OUTPUT[$i+8]=".....) (....."
			OUTPUT[$i+16]="....{   }...."
			OUTPUT[$i+24]=".....| |....."
			OUTPUT[$i+32]=".....) (....."
			OUTPUT[$i+40]="..../   \...."
			;;
			"k")
			   OUTPUT[$i]="....._+_....."
			 OUTPUT[$i+8]=".....)@(....."
			OUTPUT[$i+16]="....{@@@}...."
			OUTPUT[$i+24]=".....|@|....."
			OUTPUT[$i+32]=".....)@(....."
			OUTPUT[$i+40]="..../@@@\...."
			;;
			"K")
			   OUTPUT[$i]="....._+_....."
			 OUTPUT[$i+8]=".....) (....."
			OUTPUT[$i+16]="....{   }...."
			OUTPUT[$i+24]=".....| |....."
			OUTPUT[$i+32]=".....) (....."
			OUTPUT[$i+40]="..../   \...."
			;;
			esac
			else
			case "${POS[$i]}" in
			"0")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="             "
			OUTPUT[$i+16]="             "
			OUTPUT[$i+24]="             "
			OUTPUT[$i+32]="             "
			OUTPUT[$i+40]="             "
			;;
			"p")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="      @      "
			OUTPUT[$i+16]="     (@)     "
			OUTPUT[$i+24]="     (@)     "
			OUTPUT[$i+32]="    /@@@\    "
			OUTPUT[$i+40]="             "
			;;
			"P")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="      0      "
			OUTPUT[$i+16]="     ( )     "
			OUTPUT[$i+24]="     ( )     "
			OUTPUT[$i+32]="    /   \    "
			OUTPUT[$i+40]="             "
			;;
			"R")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="    []_[]    "
			OUTPUT[$i+16]="    \   /    "
			OUTPUT[$i+24]="     | |     "
			OUTPUT[$i+32]="     | |     "
			OUTPUT[$i+40]="    /___\    "
			;;
			"B")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="     (/)     "
			OUTPUT[$i+16]="     / \     "
			OUTPUT[$i+24]="    (   )    "
			OUTPUT[$i+32]="     ) (     "
			OUTPUT[$i+40]="    /   \    "
			;;

			"N") 
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="   _/~~~\    "
			OUTPUT[$i+16]="  [__  _}    "
			OUTPUT[$i+24]="     \ (     "
			OUTPUT[$i+32]="     | |     "
			OUTPUT[$i+40]="    /___\    "
			;;
			"r")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="    [] []    "
			OUTPUT[$i+16]="    \@@@/    "
			OUTPUT[$i+24]="     |@|     "
			OUTPUT[$i+32]="     |@|     "
			OUTPUT[$i+40]="    /@@@\    "
			;;
			"b")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="     (/)     "
			OUTPUT[$i+16]="     /@\     "
			OUTPUT[$i+24]="    (@@@)    "
			OUTPUT[$i+32]="     )@(     "
			OUTPUT[$i+40]="    /@@@\    "
			;;
			"n")
			   OUTPUT[$i]="             "
			 OUTPUT[$i+8]="   _/@@@\    "
			OUTPUT[$i+16]="  [@@@@@}    "
			OUTPUT[$i+24]="     \@(     "
			OUTPUT[$i+32]="     |@|     "
			OUTPUT[$i+40]="    /@@@\    "
			;;

			"q")
			   OUTPUT[$i]="     www     "
			 OUTPUT[$i+8]="     )@(     "
			OUTPUT[$i+16]="    {@@@}    "
			OUTPUT[$i+24]="     |@|     "
			OUTPUT[$i+32]="     )@(     "
			OUTPUT[$i+40]="    /@@@\    "
			;;
			"Q")
			   OUTPUT[$i]="     www     "
			 OUTPUT[$i+8]="     ) (     "
			OUTPUT[$i+16]="    {   }    "
			OUTPUT[$i+24]="     | |     "
			OUTPUT[$i+32]="     ) (     "
			OUTPUT[$i+40]="    /   \    "
			;;
			"k")
			   OUTPUT[$i]="     _+_     "
			 OUTPUT[$i+8]="     )@(     "
			OUTPUT[$i+16]="    {@@@}    "
			OUTPUT[$i+24]="     |@|     "
			OUTPUT[$i+32]="     )@(     "
			OUTPUT[$i+40]="    /@@@\    "
			;;
			"K")
			   OUTPUT[$i]="     _+_     "
			 OUTPUT[$i+8]="     ) (     "
			OUTPUT[$i+16]="    {   }    "
			OUTPUT[$i+24]="     | |     "
			OUTPUT[$i+32]="     ) (     "
			OUTPUT[$i+40]="    /   \    "
			esac
			fi
			}
	asciibord >> $filename
	}
}

function cursor {
case "$1" in
"v")
	tekstgen 9 116 33 "TOON VERSIE     "
	tekstgen 10 116 33  "$VERSIE"; sleep 1
	printf "\033[12;115H"
;;
"n")
	tekstgen 9 116 33 "FEN INVOEREN    "
	tekstgen 10 116 33 "INPUT FEN          ";	tekstgen 11 116 33 "Voer een fen in:"; read fen
	fenstring
	genbord
	genstukken "$STRINGA"
	appendhistory
	# zetlijst "$uci"
	printf "\033[11;115H"
;;
"q")
	tekstgen 9 116 33 "TOT ZIENS!               "; sleep 1
	quit
	printf "\033[12;115H"
;;
"s")
	genbord
	STRINGA="rnbqkbnrpppppppp00000000000000000000000000000000PPPPPPPPRNBQKBNR"
	genstukken "$STRINGA"
	appendhistory
	zetlijst "$uci"
	printf "\033[12;115H"
;;
"u")
	tekstgen 9 116 33 "UCI INVOEREN    "
	readuci
	fenuciman
	genbord
	genstukken "$STRINGC"
	appendhistory
	# zetlijst "$uci"
	oldnew
	printf "\033[12;115H"
;;
"a")
	tekstgen 9 116 33 "CONVERTEER NAAR ASCII"
	tekstgen 10 116 33 "Voer path naar te maken bestand in."
	fenascii
	printf "\033[12;115H"
;;
esac
}

function main { while [ 1 ]
do
	tekstgen 9 116 33 "WACHT OP INPUT  "
	printf "\033[52;0H\033[K"
	tekstgen 1 116 33 "(|)        (OPTIES:p1)  (x:p2 ->)
s: standaard fen laden           
n: nieuwe fen invoeren           
u: uci zet invoeren              
a: export positie als ASCII-art  
v: laat versienummer zien        
q: sluit ches.sh                 "
	printf "\033[12;115H"
	read -rs -n 1 key; cursor "$key"
done
}

clear

genhud

genbord

main "$@"